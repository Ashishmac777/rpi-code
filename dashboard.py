#For intercom first enter usbsound card to rpi ,then right click on sound icon in rpi and select usbsoundcard
#for running script type python3 dashboard.py
#install new library to python3


from socketIO_client import SocketIO               #socketIO_client library make sure that server socketio library version is 1.7.2  
from ast import literal_eval
import json                                        #json library for parsing json file
import serial										#serial library for serial communication
import time	
import RPi.GPIO as GPIO
import subprocess  							
motor_pin=12;                      
GPIO.setwarnings(False);                            #rpi use gpio 12 for master control of vehicle for safety,
GPIO.setmode(GPIO.BOARD);
GPIO.setup(12,GPIO.OUT);

ser = serial.Serial('/dev/serial0',115200)          #comport=serial0 baudrate=115200
sub_process=0
manual=0
mode=0
mode_status=0
SERVER_IP_ADDRESS="http://api.sunbots.in";           #socketio server ip address
SERVER_PORT=3000;									 #socketio port
button_status={}
def onConnect():                                     #when rpi connect to server it will reset all the button state
	print('connect');
	socketIO.emit('BUTTON_STATUS_SERVER',{'key':4,'data':{'type':0,'status':1}});
	socketIO.emit('BUTTON_STATUS_SERVER',{'key':5,'data':{'type':3,'status':0}});
	socketIO.emit('BUTTON_STATUS_SERVER',{'key':5,'data':{'type':4,'status':0}});
	socketIO.emit('BUTTON_STATUS_SERVER',{'key':5,'data':{'type':5,'status':0}});
	p = subprocess.Popen(["chromium-browser", "https://jsfiddle.net/devangbhuva97/bh350Lsr/565/"])

def onDisconnect():                                   #when rpi disconnect from server then it will pull it's gpio pin LOW so if there is any vehicle move then it will stop
	global motor_pin
	print('disconnect')
	GPIO.output(motor_pin,GPIO.LOW);
	
def on_reconnect():
	print('reconnect');
	p = subprocess.Popen(["chromium-browser", "https://jsfiddle.net/devangbhuva97/bh350Lsr/565/"])

def BUTTON_EVENT(*args):                              #
	global manual,button_status
	global mode, mode_status ,motor_pin
	global headlight_indication	
	global alert_indication;
	global sub_process
	print(args[0])
	result=json.dumps(args[0]);
	result2=json.loads(result);
	print(type(result2['key']));
	button_status[result2['data']['type']]=result2;
	#print(button_status)
	if (result2['key']==5):	
		ser.write(b"ok\n");
		ser.write(b"button_pressed=")
		ser.write(result.encode('utf-8'));
		ser.write(b'\n');

		if (result2['data']['type']==3):
			if(result2['data']['status']==1):
				print("opening google")
				#p = subprocess.Popen(["chromium-browser", "https://jsfiddle.net/devangbhuva97/bh350Lsr/501/"])
				print("opening google")
				sub_process=1;
				#alert_indication=1;
			else:
				#if(sub_process==1):
					#p.kill();
				sub_process=0;
				#alert_indication=0;
			
			
	if (result2['data']['type']==0):
		manual=0;
		if(result2['data']['status']==1):
			GPIO.output(motor_pin,GPIO.LOW);
			mode=0;
			mode_status=1;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
		else:
			GPIO.output(motor_pin,GPIO.HIGH);
			mode=0;
			mode_status=0;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
	if (result2['data']['type']==2):
		if(result2['data']['status']==1):
			GPIO.output(motor_pin,GPIO.HIGH);
			mode=2;
			mode_status=1;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
		else:
			GPIO.output(motor_pin,GPIO.LOW);
			mode=2;
			mode_status=0;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
	if (result2['data']['type']==1):
		manual=0;
		if(result2['data']['status']==1):
			GPIO.output(motor_pin,GPIO.HIGH);
			mode=1;
			mode_status=1;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
		else:
			GPIO.output(motor_pin,GPIO.LOW);
			mode=1;
			mode_status=0;
			ser.write(b"button_pressed=")
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
			
	if(result2['data']['type']==2):
		manual=1 if(result2['data']['status']==1) else  0
		print(manual)
key="arrowDown"
status=0
def ARROW_KEY_EVENT(*args):
	global key,status,motor_pin
	global manual
	#print(args[0])
	result=json.dumps(args[0]);
	result2=json.loads(result);
	if(result2['data']['key']!=key or  result2['data']["status"]!=status):
		key=result2["data"]["key"];
		status=result2["data"]["status"];
		if(manual==1):
			if(result2['data']['status']==0):
				GPIO.output(motor_pin,GPIO.LOW);
			else:
				GPIO.output(motor_pin,GPIO.HIGH);
			ser.write(b"k=");
			ser.write(result.encode('utf-8'));
			ser.write(b'\n');
			
			print(args[0])
			print(key)
			print(status)
def user_connect(*args):                                  #when new dashboard is opened all the current value is send to new dashboard
	global mode, mode_status 
	global headlight_indication
	global alert_indication,button_status;
	for i,k in button_status.items():
		socketIO.emit('BUTTON_STATUS_SERVER',k);
socketIO = SocketIO(SERVER_IP_ADDRESS,SERVER_PORT)       #conntect with server

socketIO.on('connect',onConnect)                          #event fire when rpi connect to server
socketIO.on('USER_CONNECTED',user_connect)                #event fire when any new dashboard connected to server
socketIO.on('disconnect',onDisconnect)                    #event fire when rpi disconnect to server
socketIO.on('BUTTON_STATUS_CLIENT',BUTTON_EVENT)          #event fire when button pressed from server
socketIO.on('ARROW_KEY_CLIENT',ARROW_KEY_EVENT)			  #event fire when arrow key is pressed from server
socketIO.on('reconnect', on_reconnect)                    #event fire when rpi reconnect to server


while 1:	                                              #continuous loop
	while ser.in_waiting :                                #if serial available
		data=ser.readline()                               #read data from serial 
		data=data.decode('utf-8')                         #decoding
		if(data.startswith("Ultrasonic=")):               #if data start with ultrasonic then emit event to ultrasonic server
			print(data[11:-1]);                           #extracting "ultrasonic=" from data and send to server
			socketIO.emit('ULTRASONIC_SERVER',literal_eval(data[11:-1]))
		if(data.startswith("Location=")):                  #if data start with ultrasonic then emit event to geolocation server
			print(data[9:-1])                              #extracting "Location=" from data and send to server
			socketIO.emit('GEOLOCATION_SERVER',data[9:-1]);
		if(data.startswith("Battery=")):
			#print(data[8:-1])
			socketIO.emit('BATTERY_STATUS_SERVER',literal_eval(data[8:-1]))
			socketIO.emit('NETWORK_SERVER',literal_eval("{'key':2,'data':{'type':'wifi'}}"))
		if(data.startswith("update")):                     #when arduino restart or poweron then it will send "update" to rpi so following button in dashboard reset
			print("arduino restart");
			socketIO.emit('BUTTON_STATUS_SERVER',{'key':5,'data':{'type':4,'status':0}});
			socketIO.emit('BUTTON_STATUS_SERVER',{'key':5,'data':{'type':5,'status':0}});
			socketIO.emit('BUTTON_STATUS_SERVER',{'key':4,'data':{'type':2,'status':0}});
			socketIO.emit('BUTTON_STATUS_SERVER',{'key':4,'data':{'type':1,'status':0}});
			socketIO.emit('BUTTON_STATUS_SERVER',{'key':4,'data':{'type':0,'status':1}});
	socketIO.wait(0.001);
