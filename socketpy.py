from socketIO_client import SocketIO
import json

def onConnect():
	print('connect')

def onDisconnect():
	print('disconnect')

def on_event(*args):
	
	result=json.dumps(args[0])
	result2=json.loads(result)
	print(result2['key'],end=' ')
	print(result2['data']['type'],end=' ')
	print(result2['data']['status'])

	#print(args)

def Time(*args):
        print(args)

socketIO = SocketIO('http://api.sunbots.in',3000)

socketIO.on('connect',onConnect)
socketIO.on('disconnect',onDisconnect)
socketIO.on('BUTTON_STATUS_CLIENT',on_event)
socketIO.on('time',Time)

socketIO.emit('NETWORK_SERVER',{'key':2,'data':{'type':"wifi"}}); 
socketIO.emit('GEOLOCATION_SERVER',{'key':3,'data':{'lat':23.0225,'lon':72.5714}});
socketIO.emit('ULTRASONIC_SERVER',{'key':1,'data':{'front':4,'back':1}});
socketIO.emit('BATTERY_STATUS_SERVER',{'key':6,'data':{'value':62}});
socketIO.emit('DeviceId','hi');
socketIO.wait()
